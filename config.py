import os
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = False
GOOGLEMAPS_KEY = "AIzaSyBblgxXc0sgl4y4AtIQ7gdcGGZ4vBNVudc"

WTF_CSRF_ENABLED = True
SECRET_KEY = 'you-will-never-guess'
