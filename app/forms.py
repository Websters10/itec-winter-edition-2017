from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, PasswordField, TextAreaField, IntegerField, SelectField, SubmitField
from wtforms.validators import InputRequired, Email, Length, NumberRange
from .models import User


class LoginForm(FlaskForm):
	email = StringField('email', validators=[InputRequired(), Length(max=50)])
	password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])



class RegisterForm(FlaskForm):
	email = StringField('email', validators=[InputRequired(), Email(message='Invalid email'), Length(max=50)])
	password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])
	full_name = StringField('full_name', validators=[InputRequired(), Length(max=50)])
	age = IntegerField('age', validators=[NumberRange(min=14, max=99)])
	gender = SelectField('gender', choices=[('M', 'Male'), ('F', 'Female')])


class IssuesForm(FlaskForm):
	title = StringField('title', validators=[InputRequired(), Length(max=20)])
	description = TextAreaField('description', validators=[InputRequired(), Length(min=15, max=200)])


class CommentForm(FlaskForm):
	comment = StringField('comment', validators=[InputRequired(), Length(max=140)])
	submit = SubmitField('Submit')