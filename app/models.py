from app import db


class Vote(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	author_id = db.Column(db.Integer, db.ForeignKey('user.id'))
	parent_issue = db.Column(db.Integer, db.ForeignKey('issue.id'))
	reaction = db.Column(db.Boolean)

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(50), index=True, unique=True)
	full_name = db.Column(db.String(50))
	age = db.Column(db.Integer)
	gender = db.Column(db.String(1))
	password = db.Column(db.String(80))
	isadmin = db.Column(db.Boolean)
	coords_latitude = db.Column(db.Float)
	coords_longitude = db.Column(db.Float)
	issues = db.relationship('Issue', backref='author', lazy='dynamic')
	comments = db.relationship('Comment', backref='author', lazy='dynamic')
	votes = db.relationship('Vote', backref='author', lazy='dynamic')

	@property
	def is_authenticated(self):
		return True

	@property
	def is_active(self):
		return True

	@property
	def is_anonymous(self):
		return False

	def get_id(self):
		return str(self.id)


	def __repr__(self):
		return '<User %r>' % self.id




class Issue(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(20))
	description = db.Column(db.String(300))
	coords_longitude = db.Column(db.Float)
	coords_latitude = db.Column(db.Float)
	author_id = db.Column(db.Integer, db.ForeignKey('user.id'))
	upload_time = db.Column(db.DateTime)
	state = db.Column(db.Boolean)
	comments = db.relationship('Comment', backref='parent', lazy='dynamic')
	votes = db.relationship('Vote', backref='parent', lazy='dynamic')


class Comment(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	content = db.Column(db.String(140))
	parent_issue = db.Column(db.Integer, db.ForeignKey('issue.id'))
	author_id = db.Column(db.Integer, db.ForeignKey('user.id'))