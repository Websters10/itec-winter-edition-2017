from app import app, db, lm
from .forms import LoginForm, RegisterForm, IssuesForm, CommentForm
from .models import User, Issue, Comment, Vote
from flask import render_template, url_for, flash, redirect, g, request, session, jsonify, json
from flask_login import login_user, logout_user, current_user, login_required
from datetime import datetime
from flask_googlemaps import GoogleMaps


@app.route('/')
@app.route('/index')
def index():
	page_name = 'Feed'
	user = g.user
	post_user = User()
	issue = Issue.query.all()
	return render_template('index.html', page_name=page_name, issue=issue, user=post_user)


@app.route('/login', methods=['GET', 'POST'])
def login():
	page_name = 'Sign in'
	form = LoginForm()
	if g.user is not None and g.user.is_authenticated:
		return redirect(url_for('.index'))
	if form.validate_on_submit():
		#return '<h2> Yey. Success.' + form.username.data + ' ' + form.password.data +  '</h2>'
		user = User.query.filter_by(email=form.email.data).first()
		if user:
			if user.password == form.password.data:
				login_user(user)
				return redirect(url_for('index'))
		return '<h1>Invalid username or password</h1>'

	return render_template('login.html', form=form, page_name=page_name)


@app.route('/register', methods=['GET', 'POST'])
def register():
	page_name = 'Register'
	form = RegisterForm()
	if g.user is not None and g.user.is_authenticated:
		return redirect(url_for('.index'))
	if form.validate_on_submit():
		new_user = User(password=form.password.data, email=form.email.data, full_name=form.full_name.data, gender=form.gender.data, 
			age=form.age.data, isadmin=False)
		db.session.add(new_user)
		db.session.commit()
		return redirect(url_for('index'))

	return render_template('register.html', form=form, page_name=page_name)


@lm.user_loader
def load_user(id):
	return User.query.get(int(id))


@app.before_request
def before_request():
	g.user = current_user


@app.route('/logout')
def logout():
	logout_user()
	return redirect(url_for('index'))


@app.route('/location', methods=['GET', 'POST'])
@login_required
def location():
	data = request.get_json()
	g.user.coords_latitude = data.get('lat')
	g.user.coords_longitude = data.get('longit')
	db.session.commit()
	return str(current_user.coords_longitude)


@app.route('/post_issue', methods=['GET', 'POST'])
@login_required
def post_issue():
	form = IssuesForm()
	page_name = 'Post an issue'
	time = datetime.utcnow()
	if form.validate_on_submit():
		new_issue = Issue(title=form.title.data, description=form.description.data, author_id=g.user.id, state=True, 
			upload_time=time, coords_longitude=g.user.coords_longitude, coords_latitude=g.user.coords_latitude)
		db.session.add(new_issue)
		g.user.coords_latitude = None
		g.user.coords_longitude = None
		db.session.commit()
		return redirect(url_for('index'))
	return render_template('postissue.html', page_name=page_name, form=form)


@app.route('/map', methods=['GET', 'POST'])
def show_map():
	return render_template('map.html')


@app.route('/vote', methods=['GET', 'POST'])
def vote():
	vote = request.get_json()
	req_type = vote.get('vote')
	issue_id = vote.get('issueid')
	vote_check = Vote.query.filter_by(author_id=g.user.id, parent_issue=issue_id).first()
	print(vote_check)
	if req_type == 'up' and not vote_check:
		new_vote = Vote(author_id=g.user.id, parent_issue=issue_id, reaction=True)
		db.session.add(new_vote)
		db.session.commit()
		return jsonify({'message': 'Success!'})
	elif req_type == 'down' and not vote_check:
		new_vote = Vote(author_id=g.user.id, parent_issue=issue_id, reaction=True)
		db.session.add(new_vote)
		db.session.commit()
		return jsonify({'message': 'Success!'})
	else:
		return jsonify({'message': 'Something went wrong!'})



@app.route('/issue/<id_issue>', methods=['GET', 'POST'])
def show_issue(id_issue):
	issue = Issue.query.filter_by(id=id_issue).first()
	author = User.query.filter_by(id=issue.author_id).first()
	form = CommentForm()
	if form.validate_on_submit():
		new_comment = Comment(content=form.comment.data, parent_issue=issue.id, author_id=g.user.id)
		db.session.add(new_comment)
		db.session.commit()
	comments=Comment.query.filter_by(parent_issue=issue.id)
	return render_template('showissue.html', issue=issue, author=author, form=form, comments=comments, page_name=issue.title)


@app.route('/profile/<id_user>')
def show_profile(id_user):
	user = User.query.filter_by(id=id_user).first()
	return render_template('profile.html', user=user, page_name=user.full_name)
