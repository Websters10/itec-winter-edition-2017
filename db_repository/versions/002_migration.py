from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
followers = Table('followers', pre_meta,
    Column('upvoter_id', INTEGER),
    Column('upvoted_issue_id', INTEGER),
)

downvoters = Table('downvoters', post_meta,
    Column('downvoter_id', Integer),
    Column('downvoted_issue_id', Integer),
)

upvoters = Table('upvoters', post_meta,
    Column('upvoter_id', Integer),
    Column('upvoted_issue_id', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['followers'].drop()
    post_meta.tables['downvoters'].create()
    post_meta.tables['upvoters'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['followers'].create()
    post_meta.tables['downvoters'].drop()
    post_meta.tables['upvoters'].drop()
