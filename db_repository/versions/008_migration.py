from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
issue = Table('issue', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('title', String(length=20)),
    Column('description', String(length=300)),
    Column('coords_longitude', Float),
    Column('coords_latitude', Float),
    Column('author_id', Integer),
    Column('upvotes', Integer),
    Column('downvotes', Integer),
    Column('upload_time', DateTime),
    Column('state', Boolean),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['issue'].columns['downvotes'].create()
    post_meta.tables['issue'].columns['upvotes'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['issue'].columns['downvotes'].drop()
    post_meta.tables['issue'].columns['upvotes'].drop()
