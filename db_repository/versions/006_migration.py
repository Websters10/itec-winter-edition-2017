from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
issue = Table('issue', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('title', VARCHAR(length=20)),
    Column('description', VARCHAR(length=300)),
    Column('coords_longitude', FLOAT),
    Column('coords_latitude', FLOAT),
    Column('author_id', INTEGER),
    Column('upvotes', INTEGER),
    Column('downvotes', INTEGER),
    Column('upload_time', DATETIME),
    Column('state', BOOLEAN),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['issue'].columns['downvotes'].drop()
    pre_meta.tables['issue'].columns['upvotes'].drop()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['issue'].columns['downvotes'].create()
    pre_meta.tables['issue'].columns['upvotes'].create()
